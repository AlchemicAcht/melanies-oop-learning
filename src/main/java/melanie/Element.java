package melanie;

public class Element {
    public int x, y;

    public Element( int x, int y ) {
        this.x = x;
        this.y = y;
    }

    public int width() {
        return 1;
    }

    public int height() {
        return 1;
    }

    public void draw( Canvas canvas ) {
        canvas.draw( x, y, '?' );
    }
}

