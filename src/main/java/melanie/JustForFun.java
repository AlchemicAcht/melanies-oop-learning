package melanie;

import java.util.Random;

public class JustForFun extends Element {
    public int width;
    public int height;
    public String label;

    public JustForFun( int x, int y, int width, int height, String label ) {
        super( x, y );
        this.width = width;
        this.height = height;
        this.label = label;
    }
    
    @Override
    public int width() {
        return width;
    }
    
    @Override
    public int height() {
        return height;
    }

    @Override
    public void draw( Canvas canvas ) {
        for( int yi = 0; yi < height; yi++ ) {
            for( int xi = 0; xi < width; xi++ ) {
                canvas.draw( x+xi, y+yi, '#' );
            }
        }
        int centerX = Math.max( 0, ( width - label.length() ) / 2 );
        int centerY = height/2;
        for( int labelIndex = 0; labelIndex < Math.min( label.length(), width-centerX ); labelIndex++ ) {
            canvas.draw( x+centerX+labelIndex, y+centerY, label.charAt( labelIndex ) );
        }
    }



    private static int nextInt( Random random, int minimumInclusive, int maximumExclusive ) {
        return minimumInclusive + random.nextInt( maximumExclusive - minimumInclusive );
    }
    
    public static void main( String[] args ) {
        int canvasWidth = 100;
        int canvasHeight = 30;

        Canvas canvas = new Canvas( canvasWidth, canvasHeight );
        Random random = new Random();
        int times = nextInt( random, 10, 20 );
        for( int i = 0; i < times; i++ ) {
            int x = nextInt( random, 0, canvasWidth );
            int y = nextInt( random, 0, canvasHeight );
            if( random.nextBoolean() ) {
                new Element( x, y ).draw( canvas );
            }
            else {
                new JustForFun( x, y
                    , nextInt( random, 6, 12 )
                    , nextInt( random, 2,  4 )
                    , "<" + i + ">"
                ).draw( canvas );
            }
        }
        System.out.println( canvas.asText() );
    }
}
