package melanie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Canvas {
    public static final char BACKGROUND = '/';

    private final List<StringBuilder> rows = new ArrayList<>();
    private int width = 0;
    
    public Canvas() {
    }
    public Canvas( int width, int height ) {
        ensureSize( width, height );
    }



    private String createBackgroundRow( int rowWidth ) {
        char[] backgroundRow = new char[rowWidth];
        Arrays.fill( backgroundRow, BACKGROUND );
        return new String( backgroundRow );
    }



    private void ensureSize( int x, int y ) {
        if( width <= x ) {
            String backgroundRow = createBackgroundRow( x+1 - width );
            width = x+1;
            // adjust width for old rows
            for( StringBuilder row : rows ) {
                row.append( backgroundRow );
            }
        }

        if( rows.size() <= y ) {
            // adjust height by inserting rows
            String backgroundRow = createBackgroundRow( width );
            do {
                rows.add( new StringBuilder( backgroundRow ) );
            } while( rows.size() <= y );
        }
    }



    public void draw( int x, int y, char c ) {
        ensureSize( x, y );
        rows.get( y ).setCharAt( x, c );
    }



    public String asText() {
        return String.join( "\n", rows );
    }
}
